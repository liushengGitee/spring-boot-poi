package liusheng.main;

import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.junit.jupiter.api.Test;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.util.concurrent.CountDownLatch;

/**
 * @author liusheng-小萌新
 * @version 1.0
 * @date 2020/10/12 8:07
 */

public class Main1 {
    @Test
    public void test1() throws Exception {
        XSSFWorkbook workbook = new XSSFWorkbook();
        XSSFSheet xssfSheet = workbook.createSheet("hello");
        CountDownLatch countDownLatch = new CountDownLatch(10);
        for (int i = 0; i < 10; i++) {
            int k = i;
            new Thread(()->{
                for (int j = k * 10; j < (k + 1) * 10 ; j++) {
                    XSSFRow row = xssfSheet.createRow(j);

                    XSSFCell cell = row.createCell(0);

                    cell.setCellValue(j);
                }

                countDownLatch.countDown();

            }).start();
        }

        countDownLatch.await();
        workbook.write(new FileOutputStream("1.xlsx"));
    }
}
