package liusheng.export;

public interface ConfigValue {
    Object get(String key);
}
