package liusheng.export.resolver;

import liusheng.export.InnerPOIData;
import liusheng.export.POIConstants;
import liusheng.export.POIData;
import liusheng.export.annotations.POIType;
import liusheng.export.annotations.support.FileType;
import liusheng.export.poi.handler.POIHandler;
import liusheng.export.poi.handler.POIHandlerAdapter;
import org.apache.poi.util.IOUtils;
import org.springframework.core.MethodParameter;
import org.springframework.http.HttpHeaders;
import org.springframework.util.Assert;
import org.springframework.util.StringUtils;
import org.springframework.web.context.request.NativeWebRequest;
import org.springframework.web.method.support.HandlerMethodReturnValueHandler;
import org.springframework.web.method.support.ModelAndViewContainer;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.InputStream;
import java.util.*;

public class POITypeHandlerMethodReturnValueHandler implements HandlerMethodReturnValueHandler {
    private POIHandlerAdapter poiHandlerAdapter;

    public POITypeHandlerMethodReturnValueHandler(POIHandlerAdapter poiHandlerAdapter) {
        Assert.notNull(poiHandlerAdapter, "poiHandlerAdapter为null!");
        this.poiHandlerAdapter = poiHandlerAdapter;
    }

    @Override
    public boolean supportsReturnType(MethodParameter returnType) {
        POIType methodAnnotation = returnType.getMethodAnnotation(POIType.class);
        return Objects.nonNull(methodAnnotation);
    }

    @Override
    public void handleReturnValue(Object returnValue, MethodParameter returnType, ModelAndViewContainer mavContainer, NativeWebRequest webRequest) throws Exception {

        HttpServletRequest request = webRequest.getNativeRequest(HttpServletRequest.class);
        HttpServletResponse response = webRequest.getNativeResponse(HttpServletResponse.class);
        POIType methodAnnotation = returnType.getMethodAnnotation(POIType.class);
        FileType fileType = methodAnnotation.value();
        POIHandler poiHandler = poiHandlerAdapter.select(fileType);
        if (poiHandler == null) {
            throw new RuntimeException("none PoiHandler");
        }
        boolean isListNot = Objects.isNull(returnValue) || (!List.class.isAssignableFrom(returnType.getParameterType())
                || ((List) returnValue).isEmpty());
        Object data = request.getAttribute(POIConstants.DATA);

        List<?> dataList = null;
        Class<?> clazz = null;
        Map<String, Object> params = null;
        if (data instanceof POIData) {
            POIData poiData = (POIData) data;
            dataList = poiData.getData();
            clazz = poiData.getClazz();
            params = poiData.getParams();
        }

        if (Objects.isNull(clazz) && Objects.nonNull(dataList) && !dataList.isEmpty()) {
            clazz = getClazz(dataList);
        }

        if (Objects.isNull(params)) {
            params = new HashMap<>();
        }

        if (!isListNot) {
            if (Objects.isNull(dataList)) {
                dataList = (List<?>) returnValue;
                if (Objects.isNull(clazz)) {
                    clazz = getClazz(dataList);
                }
            }
        }

        if (Objects.nonNull(clazz) && Objects.nonNull(dataList)) {
            POIData poiData = new POIData(params, dataList, clazz, returnType.getMethod());

            InnerPOIData innerPOIData = new InnerPOIData();
            innerPOIData.setPoiData(poiData);
            InputStream inputStream = poiHandler.handle(innerPOIData);
            response.setHeader(HttpHeaders.CONTENT_DISPOSITION, "attachment;filename=" + getFileName(innerPOIData, fileType));
            IOUtils.copy(inputStream, response.getOutputStream());


        } else {
            // 打印日志
            response.setHeader(HttpHeaders.CONTENT_DISPOSITION, "attachment;filename=" + "1.txt");
            response.getOutputStream().write("helloWorld.txt".getBytes());

        }
        mavContainer.setRequestHandled(true);

        // 不进行处理

    }

    private String getFileName(InnerPOIData innerPOIData, FileType fileType) {
        String fileName = innerPOIData.getFileName();
        return StringUtils.isEmpty(fileName) ? getDefaultName(fileType) : fileName;
    }

    private String getDefaultName(FileType fileType) {
        if (fileType == FileType.EXCEL) {
            return UUID.randomUUID() + ".xlsx";
        } else if (fileType == FileType.WORD) {
            return UUID.randomUUID() + ".docx";
        }
        return UUID.randomUUID().toString();
    }

    private Class<?> getClazz(List<?> dataList) {
        return dataList.get(0).getClass();
    }
}
