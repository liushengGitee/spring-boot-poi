package liusheng.export.controller;

import liusheng.export.annotations.ExcelConfig;
import liusheng.export.annotations.POIType;
import liusheng.export.annotations.support.excel.ExcelType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Arrays;
import java.util.List;

@Controller
public class ExcelController {
    @GetMapping("/hello")
    @POIType()
    @ExcelConfig(type = ExcelType.XLS)
    public Object hello() {
        List<Student> studentList = getStudents();
        return studentList;
    }
    @GetMapping("/hello1")
    @POIType()
    @ExcelConfig()
    public Object hello1() {
        return  "hello";
    }

    private List<Student> getStudents() {
        return Arrays.asList(
                new Student("hello1",1),
                new Student("hello2",2),
                new Student("hello3",3)
        );
    }
}
