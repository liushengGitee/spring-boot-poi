package liusheng.export.controller;

import liusheng.export.poi.handler.DefaultPOIHandlerAdapter;
import liusheng.export.poi.handler.DefaultWorkbookStrategy;
import liusheng.export.poi.handler.ExcelPOIHandler;
import liusheng.export.poi.handler.POIHandlerAdapter;
import liusheng.export.resolver.POITypeHandlerMethodReturnValueHandler;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;
import org.springframework.web.method.support.HandlerMethodReturnValueHandler;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import java.util.Arrays;
import java.util.List;

@Component
public class MainWebMvcConfigurer implements WebMvcConfigurer {

    @Override
    public void addReturnValueHandlers(List<HandlerMethodReturnValueHandler> handlers) {
        POITypeHandlerMethodReturnValueHandler returnValueHandler = new POITypeHandlerMethodReturnValueHandler(poiHandlerAdapter());
        handlers.add(returnValueHandler);
    }

    @Bean
    public POIHandlerAdapter poiHandlerAdapter() {
        return new DefaultPOIHandlerAdapter(Arrays.asList(new ExcelPOIHandler(new DefaultWorkbookStrategy())));
    }
}
