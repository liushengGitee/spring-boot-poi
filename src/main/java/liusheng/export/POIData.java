package liusheng.export;

import org.apache.poi.ss.formula.functions.T;

import java.lang.reflect.Method;
import java.util.List;
import java.util.Map;

public class POIData {
    private Map<String,Object> params;

    private List<?> data;

    /**
     * 如果data 数据为 null or isEmpty ，那么这个选项必须指定
     */
    private Class<?> clazz;

    private Method method;


    public Method getMethod() {
        return method;
    }

    public void setMethod(Method method) {
        this.method = method;
    }

    public POIData(List<?> data) {
        this.data = data;
    }

    public POIData(List<?> data, Class<?> clazz) {
        this.data = data;
        this.clazz = clazz;
    }

    public POIData(Map<String, Object> params, List<?> data) {
        this.params = params;
        this.data = data;
    }

    public POIData(Map<String, Object> params, List<?> data, Class<?> clazz, Method method) {
        this.params = params;
        this.data = data;
        this.clazz = clazz;
        this.method = method;
    }

    public POIData(Map<String, Object> params, List<?> data, Class<?> clazz) {
        this.params = params;
        this.data = data;
        this.clazz = clazz;
    }

    public Map<String, Object> getParams() {
        return params;
    }

    public void setParams(Map<String, Object> params) {
        this.params = params;
    }

    public List<?> getData() {
        return data;
    }

    public void setData(List<?> data) {
        this.data = data;
    }

    public Class<?> getClazz() {
        return clazz;
    }

    public void setClazz(Class<?> clazz) {
        this.clazz = clazz;
    }
}
