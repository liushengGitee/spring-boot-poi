package liusheng.export.annotations;

import liusheng.export.annotations.support.excel.ExcelType;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
public @interface ExcelConfig {

    /**
     *  type 对excel 类型的描述
     * @return
     */
    ExcelType type() default ExcelType.XLSX;

    /**
     * name excel 标题的名字 ，默认取类名
     * @return
     */
    String name() default "";

    /**
     *
     *  每一行的head名字 默认对应 实体字段 从 0 开始 一一对应
     * @return
     */

    String[] headNames() default {};

    /**
     *
     *  每一行的head名字 跟实体字段名字一一对应，优先级高于 #{headNames}
     * @return
     */
    ExcelColumn[] columns() default {};

}
