package liusheng.export.annotations;

import liusheng.export.annotations.support.FileType;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
public @interface POIType {
    FileType value() default FileType.EXCEL;
}
