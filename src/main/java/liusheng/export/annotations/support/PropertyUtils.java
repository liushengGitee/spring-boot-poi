package liusheng.export.annotations.support;

import liusheng.export.annotations.ExcelColumnHolder;
import liusheng.export.annotations.Property;
import org.springframework.core.annotation.AnnotatedElementUtils;
import org.springframework.util.StreamUtils;
import org.springframework.util.StringUtils;

import java.lang.reflect.Field;
import java.util.Date;
import java.util.Objects;

public class PropertyUtils {

    public static void parse(ExcelColumnHolder excelColumnHolder,Field field){
        if (Objects.isNull(excelColumnHolder) || Objects.isNull(field)) {
            return;
        }
        Property property = AnnotatedElementUtils.getMergedAnnotation(field, Property.class);
        if (Objects.isNull(property)) return;
        String head = property.value();
        if (StringUtils.isEmpty(excelColumnHolder.getHeadName())) {
            head = StringUtils.isEmpty(head) ? field.getName() : head ;
            excelColumnHolder.setHeadName(head);
        }
        if (Date.class.isAssignableFrom(excelColumnHolder.getPropertyType())) {
            excelColumnHolder.setFormat(property.format());
        }
    }
}
