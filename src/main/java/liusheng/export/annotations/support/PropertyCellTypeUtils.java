package liusheng.export.annotations.support;

import org.apache.poi.ss.usermodel.CellType;

import java.util.*;

public class PropertyCellTypeUtils {

    private static Map<Class<?>, CellType> cellTypeMap;

    static {
        cellTypeMap = new HashMap<>();

        cellTypeMap.put(int.class, CellType.NUMERIC);
        cellTypeMap.put(Integer.class, CellType.NUMERIC);
        cellTypeMap.put(double.class, CellType.NUMERIC);
        cellTypeMap.put(Double.class, CellType.NUMERIC);
        cellTypeMap.put(short.class, CellType.NUMERIC);
        cellTypeMap.put(Short.class, CellType.NUMERIC);
        cellTypeMap.put(byte.class, CellType.NUMERIC);
        cellTypeMap.put(Byte.class, CellType.NUMERIC);
        cellTypeMap.put(float.class, CellType.NUMERIC);
        cellTypeMap.put(Float.class, CellType.NUMERIC);
        cellTypeMap.put(long.class, CellType.NUMERIC);
        cellTypeMap.put(Long.class, CellType.NUMERIC);


        cellTypeMap.put(char.class, CellType.STRING);
        cellTypeMap.put(String.class, CellType.STRING);

        cellTypeMap.put(Date.class, CellType.STRING);

        cellTypeMap.put(boolean.class, CellType.STRING);
        cellTypeMap.put(Boolean.class, CellType.STRING);


        cellTypeMap = Collections.unmodifiableMap(cellTypeMap);
    }

    public static CellType getCellType(Class<?> clazz) {
        CellType cellType = cellTypeMap.get(clazz);

        if (Objects.isNull(cellType)) {
            cellType = CellType.STRING;
        }

        return cellType;
    }
}
