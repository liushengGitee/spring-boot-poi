package liusheng.export.annotations;

import liusheng.export.annotations.support.excel.ExcelType;

import java.util.List;
import java.util.Map;

public class ExcelConfigHolder {
    /**
     *  type 对excel 类型的描述
     * @return
     */
    private  ExcelType type;

    /**
     * name excel 标题的名字 ，默认取类名
     * @return
     */
    private  String name = "";

    /**
     *
     *  每一行的head名字 默认对应 实体字段 从 0 开始 一一对应
     * @return
     */

    private String[] headNames ;

    /**
     *
     *  每一行的head名字 跟实体字段名字一一对应，优先级高于 #{headNames}
     * @return
     */
   private  Map<String,ExcelColumnHolder> columns ;


   private String filename;

   private List<String> fieldNames;


    public List<String> getFieldNames() {
        return fieldNames;
    }

    public void setFieldNames(List<String> fieldNames) {
        this.fieldNames = fieldNames;
    }

    public String getFilename() {
        return filename;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }

    public ExcelType getType() {
        return type;
    }

    public void setType(ExcelType type) {
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String[] getHeadNames() {
        return headNames;
    }

    public void setHeadNames(String[] headNames) {
        this.headNames = headNames;
    }

    public Map<String, ExcelColumnHolder> getColumns() {
        return columns;
    }

    public void setColumns(Map<String, ExcelColumnHolder> columns) {
        this.columns = columns;
    }
}
