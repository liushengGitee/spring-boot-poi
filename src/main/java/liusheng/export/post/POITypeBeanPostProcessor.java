package liusheng.export.post;

import liusheng.export.annotations.POIType;
import org.springframework.aop.TargetSource;
import org.springframework.aop.framework.autoproxy.AbstractAutoProxyCreator;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanPostProcessor;
import org.springframework.cglib.proxy.Callback;
import org.springframework.cglib.proxy.Enhancer;
import org.springframework.cglib.proxy.MethodInterceptor;
import org.springframework.cglib.proxy.MethodProxy;
import org.springframework.core.annotation.AnnotatedElementUtils;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Controller;
import org.springframework.util.ReflectionUtils;
import org.springframework.web.bind.annotation.ResponseBody;

import java.lang.reflect.Constructor;
import java.lang.reflect.Method;
import java.util.List;

/**
 * 进行代理
 */
@Component
public class POITypeBeanPostProcessor implements BeanPostProcessor {
    @Override
    public Object postProcessAfterInitialization(Object bean, String beanName) throws BeansException {
        Class<?> clazz = bean.getClass();

        if (!(AnnotatedElementUtils.hasAnnotation(clazz, Controller.class))
        || !isNoArgConstructor(clazz)) {
            return bean;
        }


        Enhancer enhancer = new Enhancer();

        enhancer.setSuperclass(clazz);
        enhancer.setCallback(getCallBack(bean));


        return enhancer.create();
    }

    private boolean isNoArgConstructor(Class<?> clazz) {
        try {
            Constructor<?> constructor = clazz.getConstructor();
        } catch (NoSuchMethodException e) {
            return  false;
        }
        return true;
    }

    private Callback getCallBack(Object bean) {
        return new POITypeMethodInterceptor(bean);
    }

    static class POITypeClass {

    }

    static class POITypeMethodInterceptor implements MethodInterceptor {
        private Object bean;

        private POITypeClass DEFAULT = new POITypeClass();

        public POITypeMethodInterceptor(Object bean) {
            this.bean = bean;
        }

        @Override
        public Object intercept(Object o, Method method, Object[] objects, MethodProxy methodProxy) throws Throwable {
            Object invoke = method.invoke(bean, objects);
            if (!AnnotatedElementUtils.hasAnnotation(method, POIType.class)) {
                return invoke;
            }

            if (List.class.isInstance(invoke)) {
                return  invoke;
            }
            return DEFAULT;
        }
    }


}
