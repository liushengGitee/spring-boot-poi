package liusheng.export.poi.handler;

import liusheng.export.InnerPOIData;
import liusheng.export.annotations.support.FileType;

import java.io.IOException;
import java.io.InputStream;

public interface POIHandler {
    boolean support(FileType fileType);

    InputStream handle(InnerPOIData data) throws IOException;

}
