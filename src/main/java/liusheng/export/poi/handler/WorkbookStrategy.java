package liusheng.export.poi.handler;

import liusheng.export.annotations.ExcelConfigHolder;
import liusheng.export.annotations.support.excel.ExcelType;
import org.apache.poi.ss.usermodel.Workbook;

import java.util.List;

/**
 * @author Administrator
 */
public interface WorkbookStrategy {
    Workbook handle( List<?> dataList, ExcelConfigHolder excelConfigHolder);
}
