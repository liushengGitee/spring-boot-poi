package liusheng.export.poi.handler;

import com.sun.org.apache.regexp.internal.RE;
import liusheng.export.annotations.ExcelColumnHolder;
import liusheng.export.annotations.ExcelConfigHolder;
import liusheng.export.annotations.support.excel.ExcelType;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.util.CellRangeAddress;
import org.springframework.util.Assert;
import org.springframework.util.StringUtils;

import java.lang.reflect.Field;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

/**
 * @author liusheng-小萌新
 * @version 1.0
 * @date 2020/10/12 11:54
 */
public class DefaultWorkbookStrategy implements WorkbookStrategy {
    @Override
    public Workbook handle(List<?> dataList, ExcelConfigHolder excelConfigHolder) {
        Assert.notNull(dataList, "dataList is null");
        Assert.notNull(excelConfigHolder, "excelConfigHolder is null");
        ExcelType excelType = excelConfigHolder.getType();
        Assert.notNull(excelType, "excelType is null");

        int size = dataList.size();
        if (size >= Short.MAX_VALUE) {
            excelType = ExcelType.XLSX;
        }
        Workbook workbook = null;
        try {
            if (excelType == ExcelType.XLSX) {
                setFilename(excelConfigHolder, ".xlsx");
            } else {
                workbook = createHSSFWorkbook(dataList, excelConfigHolder);
                setFilename(excelConfigHolder, ".xls");
            }
        }catch (Exception e) {
            throw  new RuntimeException(e);
        }

        return workbook;
    }

    private Workbook createHSSFWorkbook(List<?> dataList, ExcelConfigHolder excelConfigHolder) throws IllegalAccessException {

        HSSFWorkbook workbook = new HSSFWorkbook();
        HSSFSheet sheet = workbook.createSheet(excelConfigHolder.getName());
        Map<String, ExcelColumnHolder> columns = excelConfigHolder.getColumns();

        HSSFRow titleRow = sheet.createRow(0);
        writeTitle(excelConfigHolder, titleRow);
        HSSFRow headRow = sheet.createRow(1);
        writeHead(excelConfigHolder, headRow);

        writeData(excelConfigHolder,dataList, sheet);
        CellRangeAddress region = new CellRangeAddress(0, 0, 0, columns.size() - 1);
        sheet.addMergedRegion(region);

        return workbook;
    }
    private void writeData(ExcelConfigHolder excelConfigHolder, List<?> dataList, HSSFSheet sheet) throws IllegalAccessException {
        Map<String, ExcelColumnHolder> columns = excelConfigHolder.getColumns();
        List<String> fieldNames = excelConfigHolder.getFieldNames();
        for (int i = 1; i <= dataList.size(); i++) {
            HSSFRow row = sheet.createRow(1 + i);
            Object o = dataList.get(i - 1);
            for (int j = 0; j < fieldNames.size(); j++) {
                ExcelColumnHolder columnHolder = columns.get(fieldNames.get(0));
                if (Objects.isNull(columnHolder)) {
                    continue;
                }
                CellType cellType = columnHolder.getCellType();
                HSSFCell cell = row.createCell(j, cellType);
                Field field = columnHolder.getField();
                Object fieldValue = field.get(o);
                if (cellType == CellType.NUMERIC) {
                    double d = 0;
                    if (Objects.nonNull(fieldValue)) {
                        d = Double.parseDouble(String.valueOf(fieldValue));
                    }
                    cell.setCellValue(d);
                } else if (cellType == CellType.BOOLEAN) {
                    Boolean b = null;
                    if (Objects.isNull(fieldValue)) {

                        cell.setCellValue("");
                        continue;
                    }
                    b = Boolean.parseBoolean(String.valueOf(fieldValue));
                    cell.setCellValue(b);
                } else if (cellType == CellType.STRING) {
                    String name = "";
                    if (Objects.nonNull(fieldValue)) {
                        name = String.valueOf(fieldValue);
                    }
                    cell.setCellValue(name);
                } else {
                    cell.setCellValue("");
                }
            }
        }

    }

    private void writeHead(ExcelConfigHolder excelConfigHolder, HSSFRow titleRow) {
        Map<String, ExcelColumnHolder> columns = excelConfigHolder.getColumns();
        List<String> fieldNames = excelConfigHolder.getFieldNames();
        for (int i = 1; i <= fieldNames.size(); i++) {
            ExcelColumnHolder excelColumnHolder = columns.get(fieldNames.get(i - 1));
            if (Objects.nonNull(excelColumnHolder)) {
                HSSFCell cell = titleRow.createCell(i -1, CellType.STRING);
                cell.setCellValue(excelColumnHolder.getHeadName());
            }
        }
    }

    private void writeTitle(ExcelConfigHolder excelConfigHolder, HSSFRow titleRow) {
        String name = excelConfigHolder.getName();

        HSSFCell cell = titleRow.createCell(0, CellType.STRING);
        cell.setCellValue(name);
    }

    private void setFilename(ExcelConfigHolder excelConfigHolder, String suffix) {
        if (StringUtils.isEmpty(excelConfigHolder.getFilename())) {
            excelConfigHolder.setFilename(excelConfigHolder.getName() + suffix);
        }
    }
}
