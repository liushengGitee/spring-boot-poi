package liusheng.export.poi.handler;

import liusheng.export.annotations.support.FileType;

public interface POIHandlerAdapter {
    POIHandler select(FileType fileType);
}
