package liusheng.export.poi.handler;

import liusheng.export.ConfigValue;
import liusheng.export.InnerPOIData;
import liusheng.export.POIData;
import liusheng.export.annotations.ExcelColumn;
import liusheng.export.annotations.ExcelColumnHolder;
import liusheng.export.annotations.ExcelConfig;
import liusheng.export.annotations.ExcelConfigHolder;
import liusheng.export.annotations.support.FileType;
import liusheng.export.annotations.support.PropertyCellTypeUtils;
import liusheng.export.annotations.support.PropertyUtils;
import liusheng.export.annotations.support.excel.ExcelType;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.util.CellRangeAddress;
import org.springframework.core.annotation.AnnotatedElementUtils;
import org.springframework.util.Assert;
import org.springframework.util.ReflectionUtils;
import org.springframework.util.StringUtils;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.*;

public class ExcelPOIHandler implements POIHandler {
    private WorkbookStrategy workbookStrategy;
    private ConfigValue configValue;

    public ExcelPOIHandler(WorkbookStrategy workbookStrategy) {
        Assert.notNull(workbookStrategy, "WorkbookStrategy is null");
        this.workbookStrategy = workbookStrategy;
    }

    public ExcelPOIHandler(WorkbookStrategy workbookStrategy, ConfigValue configValue) {
        this(workbookStrategy);
        this.configValue = configValue;
    }

    @Override
    public boolean support(FileType fileType) {
        return FileType.EXCEL == fileType;
    }

    @Override
    public InputStream handle(InnerPOIData innerPOIData) throws IOException {
        POIData data = innerPOIData.getPoiData();
        Method method = data.getMethod();
        Class<?> clazz = data.getClazz();
        List<?> dataList = data.getData();

        if (Objects.isNull(clazz) || Objects.isNull(dataList)) {
            throw new RuntimeException();
        }
        ExcelConfigHolder excelConfigHolder = null;

        List<String> fieldNames = new ArrayList<>(32);
        if (Objects.nonNull(method)) {
            ExcelConfig excelConfig = AnnotatedElementUtils.getMergedAnnotation(method, ExcelConfig.class);
            if (Objects.nonNull(excelConfig)) {
                excelConfigHolder = getExcelConfigHolder(clazz, fieldNames, excelConfig);
            }
        }

        if (Objects.isNull(excelConfigHolder)) {
            excelConfigHolder = getExcelConfigHolder(clazz, fieldNames);
        }

        afterConfig(excelConfigHolder);

        return makeInputStreamTry(dataList, excelConfigHolder, fieldNames, innerPOIData);
    }


    protected void afterConfig(ExcelConfigHolder excelConfigHolder) {
        //none
        Class<? extends ExcelConfigHolder> clazz = excelConfigHolder.getClass();

        if (Objects.nonNull(configValue)) {


        }
    }

    private InputStream makeInputStreamTry(List<?> dataList, ExcelConfigHolder excelConfigHolder, List<String> fieldNames, InnerPOIData innerPOIData) throws IOException {
        try {
            excelConfigHolder.setFieldNames(fieldNames);
            return makeInputStream(dataList, excelConfigHolder, innerPOIData);
        } catch (IllegalAccessException e) {
            throw new RuntimeException(e);
        }
    }

    private InputStream makeInputStream(List<?> dataList, ExcelConfigHolder excelConfigHolder, InnerPOIData innerPOIData) throws IOException, IllegalAccessException {

        Workbook workbook = workbookStrategy.handle(dataList, excelConfigHolder);
        innerPOIData.setFileName(innerPOIData.getFileName());
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        workbook.write(stream);
        return new ByteArrayInputStream(stream.toByteArray());
    }


    private ExcelConfigHolder getExcelConfigHolder(Class<?> clazz, List<String> fieldNames) {
        ExcelConfigHolder excelConfigHolder = new ExcelConfigHolder();
        String name = clazz.getSimpleName();
        excelConfigHolder.setName(name);
        excelConfigHolder.setType(ExcelType.XLSX);
        //excelConfigHolder.setHeadNames();
        excelConfigHolder.setColumns(convertMap(fieldNames, new String[]{}, clazz, new ExcelColumn[]{}));
        return excelConfigHolder;
    }

    private ExcelConfigHolder getExcelConfigHolder(Class<?> clazz, List<String> fieldNames, ExcelConfig excelConfig) {
        ExcelConfigHolder excelConfigHolder = new ExcelConfigHolder();
        String name = excelConfig.name();
        excelConfigHolder.setName(StringUtils.isEmpty(name) ? clazz.getSimpleName() : name);
        excelConfigHolder.setType(excelConfig.type());
        //excelConfigHolder.setHeadNames();
        excelConfigHolder.setColumns(convertMap(fieldNames, excelConfig.headNames(), clazz, excelConfig.columns()));
        return excelConfigHolder;
    }

    private Map<String, ExcelColumnHolder> convertMap(List<String> fieldNames, String[] headNames, Class<?> clazz, ExcelColumn[] columns) {
        List<Field> fields = getAllDeclaredFields(clazz, fieldNames);
        Map<String, ExcelColumnHolder> columnHolderMap = new HashMap<>(32);

        addExcelColumnHolder(fields, columnHolderMap);

        if (headNames.length != 0) {
            for (int i = 0; i < fields.size(); i++) {
                Field field = fields.get(i);
                ExcelColumnHolder excelColumnHolder = columnHolderMap.get(field.getName());
                if (Objects.nonNull(excelColumnHolder)) {
                    excelColumnHolder.setHeadName(headNames[i]);
                }
            }
        }

        if (columns.length != 0) {
            Arrays.asList(columns)
                    .forEach(column -> {
                        String name = column.name();
                        String property = column.property();
                        ExcelColumnHolder excelColumnHolder = columnHolderMap.get(property);
                        if (Objects.nonNull(excelColumnHolder)) {
                            excelColumnHolder.setHeadName(name);
                        }
                    });
        }

        return columnHolderMap;
    }

    private void addExcelColumnHolder(List<Field> fields, Map<String, ExcelColumnHolder> columnHolderMap) {
        fields.forEach(f -> {
            String name = f.getName();

            ExcelColumnHolder columnHolder = makeExcelColumn(f, name);
            PropertyUtils.parse(columnHolder, f);

            columnHolderMap.put(name, columnHolder);
        });
    }

    private ExcelColumnHolder makeExcelColumn(Field f, String name) {
        ExcelColumnHolder columnHolder = new ExcelColumnHolder();

        Class<?> clazz = f.getType();
        columnHolder.setPropertyType(clazz);
        columnHolder.setField(f);
        columnHolder.setFieldName(name);
        columnHolder.setHeadName(name);
        columnHolder.setCellType(PropertyCellTypeUtils.getCellType(clazz));
        return columnHolder;
    }

    private List<Field> getAllDeclaredFields(Class<?> clazz, List<String> fieldNames) {

        List<Field> fields = new ArrayList<>(32);

        while (clazz != Object.class) {
            Field[] fieldsArray = clazz.getDeclaredFields();
            Arrays.asList(fieldsArray)
                    .forEach(f -> {
                        ReflectionUtils.makeAccessible(f);
                        String name = f.getName();
                        if (!fieldNames.contains(name)) {
                            fields.add(f);
                            fieldNames.add(name);
                        }

                    });
            clazz = clazz.getSuperclass();
        }
        return fields;
    }
}
