package liusheng.export.poi.handler;

import liusheng.export.annotations.support.FileType;

import java.util.List;

public class DefaultPOIHandlerAdapter implements POIHandlerAdapter {
    private List<POIHandler> poiHandlers;

    public DefaultPOIHandlerAdapter(List<POIHandler> poiHandlers) {
        this.poiHandlers = poiHandlers;
    }

    @Override
    public POIHandler select(FileType fileType) {
        for (POIHandler poiHandler : poiHandlers) {
            if (poiHandler.support(fileType)) {
                return  poiHandler;
            }
        }
        return null;
    }
}
