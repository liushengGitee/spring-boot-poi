package liusheng.export;

import liusheng.export.annotations.support.excel.ExcelType;

/**
 * @author liusheng-小萌新
 * @version 1.0
 * @date 2020/10/12 11:58
 */
public class InnerPOIData {
    private POIData poiData;

    private String fileName;



    public POIData getPoiData() {
        return poiData;
    }

    public void setPoiData(POIData poiData) {
        this.poiData = poiData;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }


}
